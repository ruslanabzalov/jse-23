package tsc.abzalov.tm.exception.data;

import tsc.abzalov.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(final int index) {
        super("Index \"" + index + "\" is incorrect!");
    }

}
