package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Task;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "create-task";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create task.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("TASK CREATION\n");
        @NotNull val taskName = inputName();
        @NotNull val taskDescription = inputDescription();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        @NotNull val task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUserId(currentUserId);
        getServiceLocator().getTaskService().create(task);
        System.out.println("Task was successfully created.\n");
    }

}
