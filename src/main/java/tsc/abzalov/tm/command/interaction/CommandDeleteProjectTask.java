package tsc.abzalov.tm.command.interaction;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

@SuppressWarnings("DuplicatedCode")
public final class CommandDeleteProjectTask extends AbstractCommand {

    public CommandDeleteProjectTask(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-project-task";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project task.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("DELETE TASK FROM PROJECT\n");
        @NotNull val projectTasksService = getServiceLocator().getProjectTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        if (projectTasksService.hasData(currentUserId)) {
            System.out.println("Task");
            @NotNull val taskId = InputUtil.inputId();
            System.out.println();
            if (!Optional.ofNullable(projectTasksService.findTaskById(currentUserId, taskId)).isPresent()) {
                System.out.println("Task was not found.\n");
                return;
            }
            projectTasksService.deleteProjectTaskById(currentUserId, taskId);
            System.out.println("Task was deleted from the project.\n");
            return;
        }
        System.out.println("One of the lists is empty!");
    }

}
