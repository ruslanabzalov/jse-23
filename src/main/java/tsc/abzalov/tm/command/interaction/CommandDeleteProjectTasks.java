package tsc.abzalov.tm.command.interaction;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public final class CommandDeleteProjectTasks extends AbstractCommand {

    public CommandDeleteProjectTasks(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-project-with-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project with tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("DELETE PROJECTS WITH TASKS\n");
        @NotNull val projectTasksService = getServiceLocator().getProjectTaskService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        if (projectTasksService.hasData(currentUserId)) {
            System.out.println("Project");
            @NotNull val projectId = InputUtil.inputId();
            System.out.println();
            if (!Optional.ofNullable(projectTasksService.findProjectById(currentUserId, projectId)).isPresent()) {
                System.out.println("Project was not found.\n");
                return;
            }
            projectTasksService.deleteProjectTasksById(currentUserId, projectId);
            projectTasksService.deleteProjectById(currentUserId, projectId);
            System.out.println("Project and it tasks was successfully deleted.\n");
            return;
        }
        System.out.println("One of the lists is empty!");
    }

}
