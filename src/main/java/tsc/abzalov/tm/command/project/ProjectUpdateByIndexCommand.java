package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    public ProjectUpdateByIndexCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "update-project-by-index";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update project by index.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("EDIT PROJECT BY INDEX\n");
        @NotNull val projectService = getServiceLocator().getProjectService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            val projectIndex = inputIndex();
            @NotNull val projectName = inputName();
            @NotNull val projectDescription = inputDescription();
            System.out.println();
            @Nullable val project =
                    projectService.editByIndex(currentUserId, projectIndex, projectName, projectDescription);
            if (!Optional.ofNullable(project).isPresent()) {
                System.out.println("Project was not updated! Please, check that project exists and try again.");
                return;
            }
            System.out.println("Project was successfully updated.");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
