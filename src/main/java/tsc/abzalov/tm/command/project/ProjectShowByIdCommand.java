package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

public final class ProjectShowByIdCommand extends AbstractCommand {

    public ProjectShowByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("FIND PROJECT BY ID\n");
        @NotNull val projectService = getServiceLocator().getProjectService();
        @NotNull val currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull val projectId = inputId();
            System.out.println();
            @Nullable val project = projectService.findById(currentUserId, projectId);
            if (!Optional.ofNullable(project).isPresent()) {
                System.out.println("Searched project was not found.\n");
                return;
            }
            System.out.println((projectService.indexOf(currentUserId, project) + 1) + ". " + project + "\n");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
