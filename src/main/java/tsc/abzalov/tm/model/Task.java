package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_REFERENCE;

@Data
@EqualsAndHashCode(callSuper = true)
public final class Task extends BusinessEntity {

    @Nullable
    private String projectId;

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

    @Override
    @NotNull
    public String toString() {
        val correctProjectId = Optional.ofNullable(this.projectId).orElse(DEFAULT_REFERENCE);
        return super.toString().replace("]", "; Project ID: " + correctProjectId + "]");
    }

}
