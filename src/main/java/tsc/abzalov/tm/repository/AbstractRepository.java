package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IRepository;
import tsc.abzalov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    private final List<T> entities = new ArrayList<>();

    @Override
    public long size() {
        return entities.size();
    }

    @Override
    public boolean isEmpty() {
        return entities.isEmpty();
    }

    @Override
    public void create(@NotNull final T entity) {
        entities.add(entity);
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return entities;
    }

    @Override
    @Nullable
    public T findById(@NotNull final String id) {
        return entities.stream()
                .filter(entity -> id.equals(entity.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entities.removeIf(entity -> id.equals(entity.getId()));
    }

}
