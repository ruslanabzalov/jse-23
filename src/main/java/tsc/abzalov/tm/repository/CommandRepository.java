package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.command.AbstractCommand;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        return getAttributes(getCommands());
    }

    @Override
    @NotNull
    public Collection<String> getCommandArguments() {
        return getAttributes(arguments.values());
    }

    @Override
    @NotNull
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Override
    @NotNull
    public AbstractCommand getArgumentByName(@NotNull final String name) {
        return arguments.get(name);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @NotNull val name = command.getCommandName();
        @Nullable val argument = command.getCommandArgument();
        commands.put(name, command);
        if (Optional.ofNullable(argument).isPresent()) arguments.put(name, command);
    }

    @NotNull
    private List<String> getAttributes(@NotNull final Collection<AbstractCommand> commands) {
        @NotNull val attributes = new ArrayList<String>();
        for (@NotNull val command : commands) {
            @Nullable val attribute = command.getCommandArgument();
            if (Optional.ofNullable(attribute).isPresent()) attributes.add(attribute);
        }
        return attributes;
    }

}
