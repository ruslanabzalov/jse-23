package tsc.abzalov.tm.repository;

import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.model.Project;

public final class ProjectRepository extends AbstractBusinessEntityRepository<Project> implements IProjectRepository {
}
