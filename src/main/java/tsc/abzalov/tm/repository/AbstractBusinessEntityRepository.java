package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.model.BusinessEntity;

import java.util.List;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.enumeration.Status.*;

public abstract class AbstractBusinessEntityRepository<T extends BusinessEntity> extends AbstractRepository<T>
        implements IBusinessEntityRepository<T> {

    @NotNull
    private final List<T> entities = this.findAll();

    @Override
    public int size(@NotNull final String userId) {
        return (int) entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()))
                .count();
    }

    @Override
    public boolean isEmpty(@NotNull final String userId) {
        return entities.stream()
                .noneMatch(entity -> userId.equals(entity.getUserId()));
    }

    @Override
    public int indexOf(@NotNull final String userId, @NotNull final T entity) {
        return entities.stream()
                .filter(anotherEntity -> userId.equals(anotherEntity.getUserId()))
                .collect(Collectors.toList())
                .indexOf(entity);
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()) && id.equals(entity.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final String userId, final int index) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()))
                .collect(Collectors.toList())
                .get(index);
    }

    @Override
    @Nullable
    public T findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()) && name.equals(entity.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public T editById(
            @NotNull final String userId, @NotNull final String id,
            @NotNull final String name, @NotNull final String description
    ) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()) && id.equals(entity.getId()))
                .findFirst()
                .map(entity -> updateEntity(entity, name, description))
                .orElse(null);
    }

    @Override
    @Nullable
    public T editByIndex(
            @NotNull final String userId, final int index,
            @NotNull final String name, @NotNull final String description
    ) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()) && entity.equals(entities.get(index)))
                .findFirst()
                .map(entity -> updateEntity(entity, name, description))
                .orElse(null);

    }

    @Override
    @Nullable
    public T editByName(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()) && name.equals(entity.getName()))
                .findFirst()
                .map(entity -> {
                    entity.setDescription(description);
                    return entity;
                })
                .orElse(null);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entities.removeIf(entity -> userId.equals(entity.getUserId()));
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entities.removeIf(entity -> userId.equals(entity.getUserId()) && id.equals(entity.getId()));
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entities.removeIf(entity -> userId.equals(entity.getUserId()) && entity.equals(entities.get(index)));
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        entities.removeIf(entity -> userId.equals(entity.getUserId()) && name.equals(entity.getName()));
    }

    @Override
    @Nullable
    public T startById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()) && id.equals(entity.getId()))
                .findFirst()
                .map(entity -> {
                    if (entity.getStatus().equals(TODO)) entity.setStatus(IN_PROGRESS);
                    return entity;
                })
                .orElse(null);
    }

    @Override
    @Nullable
    public T endById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()) && id.equals(entity.getId()))
                .findFirst()
                .map(entity -> {
                    if (entity.getStatus().equals(IN_PROGRESS)) entity.setStatus(DONE);
                    return entity;
                })
                .orElse(null);
    }

    @NotNull
    private T updateEntity(@NotNull final T entity, @NotNull final String name, @NotNull final String description) {
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}
