package tsc.abzalov.tm.util;

import lombok.experimental.UtilityClass;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

import static org.apache.commons.lang3.StringUtils.isBlank;

@UtilityClass
public class InputUtil {

    public static final Scanner INPUT = new Scanner(System.in);

    public static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*" +
            "|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7" +
            "f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4]" +
            "[0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:" +
            "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";

    public static boolean isPositiveIntDigit(@NotNull final String someString) {
        val isDigit = someString.matches("^\\d+$");
        return isDigit && Integer.parseInt(someString) > 0;
    }

    public static boolean isIndexIncorrect(final int index, final int listSize) {
        return index < 0 || index > listSize - 1;
    }

    @NotNull
    public static String inputId() {
        System.out.print("Please, enter an ID: ");
        @NotNull var projectId = INPUT.nextLine();
        while (isBlank(projectId)) {
            System.out.print("ID cannot be empty! Please, enter an correct ID: ");
            projectId = INPUT.nextLine();
        }
        return projectId;
    }

    public static int inputIndex() {
        System.out.print("Please, enter an index: ");
        @NotNull var projectIndex = INPUT.nextLine();
        var isPositiveIntDigit = isPositiveIntDigit(projectIndex);
        while (!isPositiveIntDigit) {
            System.out.print("Incorrect index! Please, enter correct one: ");
            projectIndex = INPUT.nextLine();
            isPositiveIntDigit = isPositiveIntDigit(projectIndex);
        }
        return Integer.parseInt(projectIndex);
    }

    @NotNull
    public static String inputName() {
        System.out.print("Please, enter a name: ");
        @NotNull var projectName = INPUT.nextLine();
        while (isBlank(projectName)) {
            System.out.print("Name cannot be empty! Please, enter correct name: ");
            projectName = INPUT.nextLine();
        }
        return projectName;
    }

    @NotNull
    public static String inputDescription() {
        System.out.print("Please, enter a description: ");
        return INPUT.nextLine();
    }

    @NotNull
    public static String inputLogin() {
        System.out.print("Please, enter a login: ");
        @NotNull var login = INPUT.nextLine();
        while (isBlank(login)) {
            System.out.print("Login cannot be empty! Please, enter correct login: ");
            login = INPUT.nextLine();
        }
        return login;
    }

    @NotNull
    public static String inputPassword() {
        System.out.print("Please, enter a password: ");
        @NotNull var password = INPUT.nextLine();
        while (isBlank(password)) {
            System.out.print("Password cannot be empty! Please, enter correct password: ");
            password = INPUT.nextLine();
        }
        return password;
    }

    @NotNull
    public static String inputFirstName() {
        System.out.print("Please, enter first name: ");
        @NotNull var firstName = INPUT.nextLine();
        while (isBlank(firstName)) {
            System.out.print("First name cannot be empty! Please, enter correct first name: ");
            firstName = INPUT.nextLine();
        }
        return firstName;
    }

    @NotNull
    public static String inputLastName() {
        System.out.print("Please, enter a last name: ");
        return INPUT.nextLine();
    }

    @NotNull
    public static String inputEmail() {
        System.out.print("Please, enter an email: ");
        @NotNull var email = INPUT.nextLine();
        while (isBlank(email) || email.matches(EMAIL_REGEX)) {
            System.out.print("Email cannot be empty! Please, enter correct email: ");
            email = INPUT.nextLine();
        }
        return email;
    }

}
