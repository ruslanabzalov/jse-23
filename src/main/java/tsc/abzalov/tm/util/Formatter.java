package tsc.abzalov.tm.util;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;

import java.time.format.DateTimeFormatter;

import static tsc.abzalov.tm.util.LiteralConst.DATE_TIME_FORMAT;

@UtilityClass
public class Formatter {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

    @NotNull
    public static String formatBytes(final long bytes) {
        val kilobyte = 1024L;
        val megabyte = kilobyte * 1024L;
        val gigabyte = megabyte * 1024L;
        val terabyte = gigabyte * 1024L;

        if ((bytes >= 0) && (bytes < kilobyte)) return bytes + " B";
        else if ((bytes >= kilobyte) && (bytes < megabyte)) return (bytes / kilobyte) + " KB";
        else if ((bytes >= megabyte) && (bytes < gigabyte)) return (bytes / megabyte) + " MB";
        else if ((bytes >= gigabyte) && (bytes < terabyte)) return (bytes / gigabyte) + " GB";
        else if (bytes >= terabyte) return (bytes / terabyte) + " TB";
        else return bytes + " Bytes";
    }

}
