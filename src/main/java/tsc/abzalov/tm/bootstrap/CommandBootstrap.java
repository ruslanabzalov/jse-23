package tsc.abzalov.tm.bootstrap;

import lombok.Getter;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.*;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.command.auth.AuthChangePasswordCommand;
import tsc.abzalov.tm.command.auth.AuthLoginCommand;
import tsc.abzalov.tm.command.auth.AuthLogoffCommand;
import tsc.abzalov.tm.command.auth.AuthRegisterCommand;
import tsc.abzalov.tm.command.interaction.CommandAddTaskToProject;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTask;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTasks;
import tsc.abzalov.tm.command.interaction.CommandShowProjectTasks;
import tsc.abzalov.tm.command.project.*;
import tsc.abzalov.tm.command.sorting.*;
import tsc.abzalov.tm.command.system.*;
import tsc.abzalov.tm.command.task.*;
import tsc.abzalov.tm.command.user.*;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.repository.UserRepository;
import tsc.abzalov.tm.service.*;

import java.util.Arrays;
import java.util.Optional;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

/**
 * Bootstrap application class.
 * @author Ruslan Abzalov.
 */
@Getter
public final class CommandBootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    public CommandBootstrap() {
        // Commands registration.
        registerCommand(new SystemAboutCommand(this));
        registerCommand(new SystemArgumentsCommand(this));
        registerCommand(new SystemCommandsCommand(this));
        registerCommand(new SystemExitCommand(this));
        registerCommand(new SystemHelpCommand(this));
        registerCommand(new SystemInfoCommand(this));
        registerCommand(new SystemVersionCommand(this));
        registerCommand(new ProjectCreateCommand(this));
        registerCommand(new ProjectDeleteAllCommand(this));
        registerCommand(new ProjectDeleteByIdCommand(this));
        registerCommand(new ProjectDeleteByIndexCommand(this));
        registerCommand(new ProjectDeleteByNameCommand(this));
        registerCommand(new ProjectEndByIdCommand(this));
        registerCommand(new ProjectShowAllCommand(this));
        registerCommand(new ProjectShowByIdCommand(this));
        registerCommand(new ProjectShowByIndexCommand(this));
        registerCommand(new ProjectShowByNameCommand(this));
        registerCommand(new ProjectStartByIdCommand(this));
        registerCommand(new ProjectUpdateByIdCommand(this));
        registerCommand(new ProjectUpdateByIndexCommand(this));
        registerCommand(new ProjectUpdateByNameCommand(this));
        registerCommand(new TaskCreateCommand(this));
        registerCommand(new TaskDeleteAllCommand(this));
        registerCommand(new TaskDeleteByIdCommand(this));
        registerCommand(new TaskDeleteByIndexCommand(this));
        registerCommand(new TaskDeleteByNameCommand(this));
        registerCommand(new TaskEndByIdCommand(this));
        registerCommand(new TaskShowAllCommand(this));
        registerCommand(new TaskShowByIdCommand(this));
        registerCommand(new TaskShowByIndexCommand(this));
        registerCommand(new TaskShowByNameCommand(this));
        registerCommand(new TaskStartByIdCommand(this));
        registerCommand(new TaskUpdateByIdCommand(this));
        registerCommand(new TaskUpdateByIndexCommand(this));
        registerCommand(new TaskUpdateByNameCommand(this));
        registerCommand(new CommandAddTaskToProject(this));
        registerCommand(new CommandDeleteProjectTask(this));
        registerCommand(new CommandDeleteProjectTasks(this));
        registerCommand(new CommandShowProjectTasks(this));
        registerCommand(new SortingProjectsByEndDateCommand(this));
        registerCommand(new SortingProjectsByNameCommand(this));
        registerCommand(new SortingProjectsByStartDateCommand(this));
        registerCommand(new SortingProjectsByStatusCommand(this));
        registerCommand(new SortingTasksByEndDateCommand(this));
        registerCommand(new SortingTasksByNameCommand(this));
        registerCommand(new SortingTasksByStartDateCommand(this));
        registerCommand(new SortingTasksByStatusCommand(this));
        registerCommand(new AuthRegisterCommand(this));
        registerCommand(new AuthLoginCommand(this));
        registerCommand(new AuthLogoffCommand(this));
        registerCommand(new AuthChangePasswordCommand(this));
        registerCommand(new UserShowInfoCommand(this));
        registerCommand(new UserChangeInfoCommand(this));
        registerCommand(new UserDeleteByIdCommand(this));
        registerCommand(new UserDeleteByLoginCommand(this));
        registerCommand(new UserLockUnlockByIdCommand(this));
        registerCommand(new UserLockUnlockByLoginCommand(this));
    }

    // Default users initialization.
    {
        try {
            userService.create("admin", "admin", ADMIN, "Admin", "", "admin@mail.com");

            @Nullable val admin = userService.findByLogin("admin");
            Optional.ofNullable(admin).orElseThrow(UserIsNotExistException::new);

            initProject("Main Project", "Main Admin Project", admin.getId());
            initTask("Main Task", "Main Admin Task", admin.getId());

            userService.create("test", "test", "Test", "", "test@mail.com");

            @Nullable val testUser = userService.findByLogin("test");
            Optional.ofNullable(testUser).orElseThrow(UserIsNotExistException::new);

            initProject("Simple Project", "Simple Test User Project", testUser.getId());
            initTask("Simple Task", "Simple Test User Task", testUser.getId());
        } catch (Exception exception) {
            loggerService.error(exception);
        }
    }

    /**
     * Application main method with infinite loop.
     * @param args Commandline arguments.
     * @author Ruslan Abzalov.
     */
    public void run(@NotNull final String... args) {
        System.out.println();
        try {
            if (areArgExists(args)) return;
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
            return;
        }

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");
        System.out.println("Please, login/register new user to start work with the application.\n");

        @NotNull val availableStartupCommands = Arrays.asList("register", "login", "logoff", "help", "exit");
        @Nullable String commandName;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();

            if (isEmpty(commandName)) continue;

            if (authService.isSessionInactive()) {
                val isCommandAvailable = availableStartupCommands.contains(commandName);
                if (!isCommandAvailable) {
                    System.out.println("Session is inactive! Please, register new user or login.");
                    continue;
                }

                try {
                    loggerService.command(commandName);
                    @NotNull val command = commandService.getCommandByName(commandName);
                    command.execute();
                } catch (@NotNull final Exception exception) {
                    loggerService.error(exception);
                }
            }
            else {
                try {
                    @NotNull val command = commandService.getCommandByName(commandName);
                    @NotNull val commandRoles = command.getRoles();
                    @NotNull val currentUserRole = authService.getCurrentUserRole();
                    val canUserExecuteCommand = commandRoles.contains(currentUserRole);
                    if (canUserExecuteCommand) {
                        loggerService.command(commandName);
                        command.execute();
                        continue;
                    }
                    throw new AccessDeniedException();
                } catch (@NotNull final Exception exception) {
                    loggerService.error(exception);
                }
            }
        }
    }

    /**
     * Register command method.
     * @param command Registered command.
     * @author Ruslan Abzalov.
     */
    private void registerCommand(@NotNull final AbstractCommand command) {
        commandService.add(command);
    }

    /**
     * Project initialization method.
     * @param projectName Project name.
     * @param projectDescription Project description.
     * @param userId Project user id.
     * @author Ruslan Abzalov.
     */
    private void initProject(
            @NotNull final String projectName, @NotNull final String projectDescription, @NotNull final String userId
    ) {
        @NotNull val project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUserId(userId);
        projectService.create(project);
    }

    /**
     * Task initialization method.
     * @param taskName Task name.
     * @param taskDescription Task description.
     * @param userId Task user id.
     */
    private void initTask(
            @NotNull final String taskName, @NotNull final String taskDescription, @NotNull final String userId
    ) {
        @NotNull val task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUserId(userId);
        taskService.create(task);
    }

    /**
     * Execute command if commandline arguments exist.
     * @param args Commandline arguments.
     * @return Was command executed.
     */
    private boolean areArgExists(@NotNull final String... args) {
        if (isEmpty(args)) return false;
        @NotNull val arg = args[0];
        if (isEmpty(arg)) return false;
        commandService.getArgumentByName(arg).execute();
        return true;
    }

}
