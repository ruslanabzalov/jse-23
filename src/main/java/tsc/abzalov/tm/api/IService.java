package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Base service interface.
 * @param <T> Type.
 */
public interface IService<T> {

    long size();

    boolean isEmpty();

    void create(@NotNull T entity);

    @NotNull
    List<T> findAll();

    @Nullable
    T findById(@NotNull String id);

    void clear();

    void removeById(@NotNull String id);

}
