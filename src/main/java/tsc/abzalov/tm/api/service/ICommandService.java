package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.exception.general.IncorrectCommandException;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArguments();

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getArgumentByName(@NotNull String name);

    void add(@NotNull AbstractCommand command);

}
