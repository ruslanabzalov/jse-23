package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

public interface IAuthService {

    void register(
            @NotNull String login, @NotNull String password,
            @NotNull String firstName, @Nullable String lastName,
            @NotNull String email
    );

    void login(@NotNull String login, @NotNull String password);

    void logoff();

    boolean isSessionActive();

    boolean isSessionInactive();

    @NotNull
    String getCurrentUserId();

    @NotNull
    String getCurrentUserLogin();

    @NotNull
    Role getCurrentUserRole();

}
